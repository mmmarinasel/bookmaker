import SwiftUI

extension ContentView {
    var firstCell: some View {
        VStack(alignment: .leading) {
            CellHeader(text: TextConstants.avgRatio)
            VStack {
                HStack {
                    RatioGraph(
                        color: .shinyShamrock,
                        width: ratioBgSize * (viewModel.winning / (viewModel.losing + viewModel.refund))
                    )
                    .background {
                        GeometryReader { geometry in
                            Color.clear
                                .onAppear {
                                    ratioBgSize = geometry.size.width
                                }
                        }
                    }
                    BetLabel(digit: viewModel.winning, text: TextConstants.winning)
                }
                HStack {
                    RatioGraph(
                        color: .paleCarmine,
                        width: ratioBgSize * (viewModel.losing / (viewModel.winning + viewModel.refund))
                    )
                    BetLabel(digit: viewModel.losing, text: TextConstants.losing)
                }
                HStack {
                    RatioGraph(
                        color: .spanishGray,
                        width: ratioBgSize * (viewModel.refund / (viewModel.losing + viewModel.winning))
                    )
                    BetLabel(digit: viewModel.refund, text: TextConstants.refund)
                }
            }
        }
        .cellBorder()
    }

    var secondCell: some View {
        VStack(alignment: .leading) {
            CellHeader(text: TextConstants.winningsOrLosings)
            logoWithBets
            HStack {
                ZStack {
                    betsBgGraph
                    HStack(spacing: padding) {
                        BetsGraph(
                            bets: viewModel.winningBookmaker,
                            percents: viewModel.winningBookmaker.getBetPercent(from: viewModel.bets),
                            width: (betsBgSize - 2 * padding) * CGFloat((Double(viewModel.winningBookmaker) / Double(viewModel.bets))),
                            color: .shinyShamrock
                        )
                        BetsGraph(
                            bets: viewModel.losingBookmaker,
                            percents: viewModel.losingBookmaker.getBetPercent(from: viewModel.bets),
                            width: (betsBgSize - 2 * padding) * CGFloat((Double(viewModel.losingBookmaker) / Double(viewModel.bets))),
                            color: .paleCarmine
                        )
                        BetsGraph(
                            bets: viewModel.refundBookmaker,
                            percents: viewModel.refundBookmaker.getBetPercent(from: viewModel.bets),
                            width: (betsBgSize - 2 * padding) * CGFloat((Double(viewModel.refundBookmaker) / Double(viewModel.bets))),
                            color: .spanishGray
                        )
                    }
                }
            }
            .frame(maxWidth: .infinity)
        }
        .cellBorder()
    }

    var logoWithBets: some View {
        HStack(spacing: 10) {
            Image("logo")
                .resizable()
                .frame(width: 60, height: 30)
            Text(viewModel.bets.convert())
                .font(.subheadline)
                .fontWeight(.medium)
        }
    }

    var betsBgGraph: some View {
        RoundedRectangle(cornerRadius: 1.5)
            .fill(.clear)
            .frame(height: 40)
            .background {
                GeometryReader { geometry in
                    Color.clear
                        .onAppear {
                            betsBgSize = geometry.size.width
                        }
                }
            }
    }
}
