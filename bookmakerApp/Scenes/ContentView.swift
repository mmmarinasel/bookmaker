import SwiftUI

struct ContentView: View {
    @StateObject var viewModel = ViewModel()
    @State var ratioBgSize: CGFloat = 0
    @State var betsBgSize: CGFloat = 0
    let padding: CGFloat = 5

    var body: some View {
        VStack {
            firstCell
            secondCell
        }
        Spacer()
    }
}

#Preview {
    ContentView()
}
