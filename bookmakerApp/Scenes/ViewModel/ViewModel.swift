import Foundation

final class ViewModel: ObservableObject {
    var winning: Double = 1.94
    var losing: Double = 2.17
    var refund: Double = 1.26

    var winningBookmaker: Int = 16
    var losingBookmaker: Int = 11
    var refundBookmaker: Int = 5

    var bets: Int {
        winningBookmaker + losingBookmaker + refundBookmaker
    }
}
