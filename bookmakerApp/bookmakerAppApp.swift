import SwiftUI

@main
struct bookmakerAppApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
