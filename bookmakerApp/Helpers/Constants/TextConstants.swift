import Foundation

struct TextConstants {
    static let winningsOrLosings = "Выигрыши/проигрыши по букмекерам"
    static let avgRatio = "Средние коэффициенты"
    static let winning = "Выигрыш"
    static let losing = "Проигрыш"
    static let refund = "Возврат"
}
