import SwiftUI

extension View {
    func cellBorder() -> some View {
        self.modifier(BorderModifier())
    }
}
