import SwiftUI

struct BorderModifier: ViewModifier {
    func body(content: Content) -> some View {
        content
            .frame(maxWidth: .infinity)
            .padding()
            .overlay {
                Rectangle()
                    .fill(.clear)
                    .border(Color.black, width: 1)
            }
            .padding()
    }
}
