import SwiftUI

struct CellHeader: View {
    let text: String

    var body: some View {
        Text(text)
            .font(.callout)
            .fontWeight(.semibold)
            .frame(alignment: .leading)
    }
}
