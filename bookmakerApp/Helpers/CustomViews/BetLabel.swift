import SwiftUI

struct BetLabel: View {
    let digit: Double
    let text: String

    var body: some View {
        HStack {
            Text(text)
                .font(.caption)
                .foregroundStyle(.gray)
            Spacer()
            Text(String(format: "%.2f", digit))
                .font(.caption)
                .fontWeight(.semibold)
        }
        .frame(width: 100, alignment: .leading)
    }
}
