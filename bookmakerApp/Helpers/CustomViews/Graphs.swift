import SwiftUI

struct RatioGraph: View {
    let color: Color
    let width: CGFloat

    var body: some View {
        RoundedRectangle(cornerRadius: 1.5)
            .frame(height: 10)
            .foregroundStyle(.gray)
            .opacity(0.25)
            .overlay(alignment: .leading) {
                RoundedRectangle(cornerRadius: 1.5)
                    .fill(color)
                    .frame(width: width)
            }
    }
}

struct BetsGraph: View {
    var bets: Int
    var percents: Int
    var width: CGFloat
    var color: Color

    var body: some View {
        VStack(alignment: color == .spanishGray ? .trailing : .leading) {
            RoundedRectangle(cornerRadius: 1.5)
                .frame(width: width > 0 ? width : 0, height: 10)
                .foregroundStyle(color)
            HStack(spacing: 2) {
                Text("\(bets)")
                    .fontWeight(.medium)
                Text("(\(percents)%)")
                    .fontWeight(.medium)
            }
            .font(.system(size: 10))
            .minimumScaleFactor(0.6)
        }
    }
}
