import Foundation

extension Int {
    func convert() -> String {
        var temp = self % 100

        if temp >= 5 && temp <= 20 {
            return "\(self) ставок"
        }

        temp %= 10

        switch temp {
        case 1:
            return "\(self) ставка"
        case 2, 3, 4:
            return "\(self) ставки"
        default:
            return "\(self) ставок"
        }
    }

    func getBetPercent(from totalCount: Int) -> Self {
        let rounded = Double(self) / Double(totalCount) * 100

        return Int(rounded.rounded())
    }
}
